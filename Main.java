package com.ignatev;

public class Main
{
    public static void main(String[] args)
        {
            int sumInt = 0;
            double sumDouble = 0;
            int countDouble = 0;

            for(int i = 0; i < args.length; i++) {
                if (args[i].matches("\\d+\\.\\d+")) { 
                    double d = Double.parseDouble(args[i]);
                    System.out.println(d + " is double");
                    sumDouble += d;
                    countDouble++;
                }
                else {
                    int k = Integer.parseInt(args[i]);
                    System.out.println(k + " is int");
                    sumInt += k;
                }
            }
            double average = sumDouble / countDouble;
            System.out.println(sumInt + " sumInt");
            System.out.println(average + " averageDouble");
        }
}
